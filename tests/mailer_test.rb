require "./mailer.rb"
require "minitest/autorun"
require "minitest/mock"
require 'securerandom'

class MailerTest < Minitest::Test
  def setup
    @mock_client = Minitest::Mock.new
    @mock_logger = Minitest::Mock.new
    @alert = {user: {email: "test@test.com", name: "John Doe"},
             unique_id: SecureRandom.hex,
             rentals: [
               {
                 source: "Blocket",
                 location: "Gamla Stan",
                 type: "Apartment",
                 rent: 10000,
                 rooms: 3,
                 size: 0
               } ] }
    @mailer = Mailer.new(@mock_client, @mock_logger)
    @email = @mailer.generate_mail(@alert)
  end

  def test_send_alert_mail
    @mock_client.expect :send_email, nil, [@email]

    @mailer.send_alert_mail(@alert)

    @mock_client.verify
  end

  def test_log_error_on_ses_error
    error = Aws::SES::Errors::ServiceError.new(nil, "service down")
    @mock_client.expect :send_email, nil do
      raise error
    end

    @mock_logger.expect :Error, nil, ["Error sending mail: #{error}"]
    @mailer.send_alert_mail(@alert)

    @mock_client.verify
    @mock_logger.verify
  end
end
