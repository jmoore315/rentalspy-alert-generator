require "./alert_generator.rb"
require "minitest/autorun"
require "minitest/mock"

class AlertGeneratorTest < Minitest::Test
  def setup
    @mock_mongo_client = Minitest::Mock.new
    @mock_aws_client = Minitest::Mock.new
    @mock_logger = Minitest::Mock.new

    @alert_generator = AlertGenerator.new(@mock_mongo_client, @mock_aws_client, @mock_logger)

    @no_filters_user = { name: "John Doe", email: "test@test.com", referral_id: '1',
             blocket: true, bostad_direkt: true, hittarum: true, bopunkten: true,
             min_size: 0, min_rooms: 0, max_rent: 0,
             in_the_city: true, near_the_city: true, far_from_the_city: true,
             room: true, entire_apartment: true, house: true }

    @low_rent_user      = @no_filters_user.clone.update(max_rent: 3000, referral_id: '2')
    @big_size_user      = @no_filters_user.clone.update(min_size: 100, referral_id: '3')
    @many_rooms_user    = @no_filters_user.clone.update(min_rooms: 3, referral_id: '4')
    @city_user          = @no_filters_user.clone.update(far_from_the_city: false, near_the_city: false,
                                                        referral_id: '5')
    @country_house_user = @no_filters_user.clone.update(in_the_city: false, near_the_city: false,
                                                        referral_id: '6')
    @blocket_user       = @no_filters_user.clone.update(hittarum: false, bostad_direkt: false, bopunkten: false,
                                                        referral_id: '7')
    @no_alerts_user     = @no_filters_user.clone.update(hittarum: false, bostad_direkt: false, bopunkten: false,
                                                        blocket: false, referral_id: '8')

    base_rental = { source: "Blocket", size: 0, rooms: 0, rent: 0,
                    location_type: :in_the_city, type: :room }
    @blocket_city_rental_high_price = base_rental.clone.update(rent: 20000)
    @hittarum_far_from_city_house = base_rental.clone.update(rent: 7000,
                                                            type: :house,
                                                            source: "Hittarum",
                                                            location_type: :far_from_the_city,
                                                            size: 200,
                                                            rooms: 4)
    @bostad_direct_room = base_rental.clone.update(rent: 3000,
                                                  source: "Hittarum",
                                                  location_type: :near_the_city,
                                                  size: 20,
                                                  rooms: 1)
    @rentals = [@blocket_city_rental_high_price, @hittarum_far_from_city_house, @bostad_direct_room]
  end


  def test_fits_filters
    @rentals.each do |rental|
      assert_equal @alert_generator.fits_filters?(rental, @no_filters_user), true
      assert_equal @alert_generator.fits_filters?(rental, @no_alerts_user), false
    end

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @low_rent_user), false
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @low_rent_user), false
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @low_rent_user), true

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @big_size_user), false
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @big_size_user), true
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @big_size_user), false

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @many_rooms_user), false
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @many_rooms_user), true
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @many_rooms_user), false

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @city_user), true
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @city_user), false
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @city_user), false

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @country_house_user), false
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @country_house_user), true
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @country_house_user), false

    assert_equal @alert_generator.fits_filters?(@blocket_city_rental_high_price, @blocket_user), true
    assert_equal @alert_generator.fits_filters?(@hittarum_far_from_city_house, @blocket_user), false
    assert_equal @alert_generator.fits_filters?(@bostad_direct_room, @blocket_user), false
  end

  def test_generate_alerts_for_users
    users = [@no_filters_user, @no_alerts_user, @low_rent_user, @big_size_user, @many_rooms_user, @city_user,
             @country_house_user, @blocket_user]
    @mock_logger.expect :info, nil do |args|
      return nil
    end

    generated_alerts = @alert_generator.generate_alerts_for_users(@rentals, users).map{ |a| a.delete(:unique_id) }

    expected_alerts = [
      {user: alert_user(@no_filters_user), rentals: [@rentals] },
      {user: alert_user(@low_rent_user), rentals: [@bostad_direct_room] },
      {user: alert_user(@big_size_user), rentals: [@hittarum_far_from_city_house] },
      {user: alert_user(@many_rooms_user), rentals: [@hittarum_far_from_city_house] },
      {user: alert_user(@city_user), rentals: [@blocket_city_rental_high_price] },
      {user: alert_user(@country_house_user), rentals: [@hittarum_far_from_city_house] },
      {user: alert_user(@blocket_user), rentals: [@blocket_city_rental_high_price] }
    ]

    assert_same_elements(generated_alerts, expected_alerts)
  end

  private
    def alert_user(user)
      {email: user[:email], name: user[:name], referral_id: user[:referral_id]}
    end
end
