require 'open-uri'
require 'nokogiri'
require 'set'

class HittarumScraper
  def initialize(mongo_client, logger)
    @stockholm_url = "http://www.hittarum.nu/hitta-lagenhet"
    @near_the_city = ["Årsta", "Älvsjö", "Bromma", "Bagarmossen", "Bandhagen","Danderyd", "Djursholm", "Enskededalen", "Gröndal","Gullmarsplan", "Hammarby", "Johanneshov", "Nacka", "Solna", "Stora Essingen", "Sundbyberg","Täby", "Upplands Väsby", "Vällingby, Spånga", "Lidingö"]
    @far_from_the_city = ["Sundbyberg", "Tyresö","Jordbro","Handen", "Botkyrka","Ekerö", "Enskede", "Farsta", "Farsta / Ågesta", "Gamla Enskede", "Haninge","Huddinge","Hägersten","Hässelby", "Ingarö", "Järfälla","Kista", "Kummelnäs", "Norrtälje","Nykvarn","Nynäshamn","Salem", "Saltsjö-Duvnäs","Segeltorp", "Skarpnack", "Sollentuna", "Spanga", "Tyresö", 
                 "Upplands-Bro","Vällingby","Värmdö", "Vallentuna","Vaxholm","Värmdö","Österåker"]
    @logger = logger
    @client = mongo_client
  end

  def scrape
    puts "HittarumScraping"
    begin
    page = Nokogiri::HTML(open(@stockholm_url))

    listings = page.css(".item-list li")

    rentals = []
    listings.each do |listing|

      url = listing.css("a").attribute('href').value rescue nil
      label = listing.css("a > p")[0].text rescue nil
      location = label[label.index(',')+1,label.length].strip.capitalize rescue nil
      rooms = label[0, label.index(':')].strip.to_f rescue nil
      size = listing.css("a > p")[1].text.gsub('m','').to_i rescue nil
      rent = listing.css("a > p")[2].text.gsub('kr/mån','').gsub(' ', '').to_i rescue nil
      title = listing.css("a h3").text rescue nil

      rental = {source: "Hittarum",
                url: url,
                text: title,
                date: DateTime.new,
                rooms: rooms,
                rent: rent,
                size: size,
                location: location,
                location_type: get_location_type(location),
                type: get_type(title, rooms)
      }
      rental.each {|k,v| @logger.warn "#{k} nil for url #{rental[:url]}" if v.nil?}
      rentals << rental unless rental[:url].nil?
    end

    urls = rentals.map{|r| r[:url] }
    existing_urls = @client[:rentals].find( {url: { "$in" => urls } }).projection(url: 1).to_a.map { |r| r[:url] }

    rentals.find_all { |r| !existing_urls.include?(r[:url]) && valid_location(r[:location])}
    rescue
      []
    end
  end

  def valid_location(location)
    if location
      lower_loc = location.downcase
      lower_loc.include? "stockholm" || @near_the_city.map(&:downcase).include?(lower_loc) || @far_from_the_city.map(&:downcase).include?(lower_loc)
    else
        false
    end
  end

  def get_location_type(location)
    if location
      if location.include? "Stockholm"
        return :in_the_city
      elsif @near_the_city.map(&:downcase).include?(location.downcase)
        return :near_the_city
      else
        return :far_from_the_city
      end
    end
    return :near_the_city
  end

  def get_type(title, rooms)
    if title.downcase.include? 'rum'
      :room
    else
      :apartment
    end
  end
end
