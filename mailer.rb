require 'mail'
require "erb"
require 'aws-sdk'

class Mailer
  def initialize(aws_client, logger)
    @aws_client = aws_client
    @logger = logger
  end

  def send_alert_mail(alert)
    mail = generate_mail(alert)
    begin
      @aws_client.send_email(mail)
      #TODO record email sent event and # alerts sent for user in DB
    rescue Aws::SES::Errors::ServiceError => e
      @logger.Error "Error sending mail: #{e}"
    end
  end

  def generate_mail(alert)
    {
      source: "RentalSpy <alerts@rentalspy.se>",
      destination: {
        to_addresses: [alert[:user][:email]]
      },
      message: {
        subject: {
          data: "Rental Alert!",
          charset: "UTF-8",
        },
        body: {
          text: {
            data: ERB.new(File.read("alerts_email.txt.erb")).result(alert.instance_eval { binding }),
            charset: "UTF-8",
          },
          html: {
            data: ERB.new(File.read("alerts_email.html.erb")).result(alert.instance_eval { binding }),
            charset: "UTF-8"
          }
        }
      }
    }
  end
end
