# README #

Webcrawler / email notifier for rentalspy.se 

Dependencies:

* Mongo
* AWS SES account and credentials

### Running:
To run standalone:

* set mongo and AWS ENV variables OR
* create `.env` (see `.env.example`)
* run `$ bundle exec ruby generate_alerts.rb`

To run tests:

* `$ rake test`

To run dockerized alert & mongo:

* `$ docker compose up`