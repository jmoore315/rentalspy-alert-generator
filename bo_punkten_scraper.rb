require 'open-uri'
require 'nokogiri'
require 'set'

class BoPunktenScraper
  def initialize(mongo_client, logger)
    @stockholm_url = "https://www.bopunkten.se/stockholm/"
    @in_the_city= ["City", "Djurgården","Gamla stan", "Gärdet","Kungsholmen","Liljeholmen", "Reimersholme", "Södermalm", "Vasastan", "Östermalm"]
    @base_url = "https://www.bopunkten.se"
    @near_the_city = ["Årsta", "Älvsjö", "Bromma", "Bagarmossen", "Bandhagen","Danderyd", "Djursholm", "Enskededalen", "Gröndal","Gullmarsplan", "Hammarby", "Johanneshov", "Nacka", "Solna", "Stora Essingen", "Sundbyberg","Täby", "Upplands Väsby", "Vällingby, Spånga", "Lidingö"]
    @far_from_the_city = ["Sundbyberg", "Tyresö","Jordbro","Handen", "Botkyrka","Ekerö", "Enskede", "Farsta", "Farsta / Ågesta", "Gamla Enskede", "Haninge","Huddinge","Hägersten","Hässelby", "Ingarö", "Järfälla","Kista", "Kummelnäs", "Norrtälje","Nykvarn","Nynäshamn","Salem", "Saltsjö-Duvnäs","Segeltorp", "Skarpnack", "Sollentuna", "Spanga", "Tyresö", 
                 "Upplands-Bro","Vällingby","Värmdö", "Vallentuna","Vaxholm","Värmdö","Österåker"]
    @logger = logger
    @client = mongo_client
  end

  def scrape
    puts "BoPunktenScraping"
    begin
    page = Nokogiri::HTML(open(@stockholm_url))

    listings = page.css("table.overview tbody tr")

    rentals = []
    listings.each do |listing|

      url = @base_url + listing.css("td a").attribute('href').value rescue nil
      title = listing.css("td a").text rescue nil
      location = listing.css("td")[3].text rescue nil
      rooms = listing.css("td")[4].text.to_i rescue nil
      size = /\d+/.match(listing.css("td")[5]).to_s.to_i  rescue nil
      rent = listing.css("td")[6].text.to_i rescue nil
      type = listing.css("td")[8].text rescue nil

      rental = {source: "BoPunkten",
                url: url,
                text: title,
                date: DateTime.new,
                rooms: rooms,
                rent: rent,
                size: size,
                location: location,
                location_type: get_location_type(location),
                type: get_type(type)
      }
      rental.each {|k,v| @logger.warn "#{k} nil for url #{rental[:url]}" if v.nil?}
      rentals << rental unless rental[:url].nil?
    end

    urls = rentals.map{|r| r[:url] }
    existing_urls = @client[:rentals].find( {url: { "$in" => urls } }).projection(url: 1).to_a.map { |r| r[:url] }

    rentals.find_all { |r| !existing_urls.include?(r[:url]) }
    rescue
      []
    end
  end

  def get_location_type(location)
    if location
      if @in_the_city.map(&:downcase).include?(location.downcase)
        return :in_the_city
      elsif @near_the_city.map(&:downcase).include?(location.downcase)
        return :near_the_city
      else
        return :far_from_the_city
      end
    end
    return :near_the_city
  end

  def get_type(type)
    if type.downcase.include? 'rum'
      :room
    else
      :apartment
    end
  end
end
