FROM ruby:2.2.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN mkdir /alert-generator
WORKDIR /alert-generator
ADD Gemfile /alert-generator/Gemfile
ADD Gemfile.lock /alert-generator/Gemfile.lock
RUN bundle install
ADD . /alert-generator

CMD ["bundle", "exec", "ruby", "generate_alerts.rb"]
