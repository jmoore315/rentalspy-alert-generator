require 'mongo'
require 'open-uri'
require 'nokogiri'
require 'set'

BLOCKET_STOCKHOLM_URL = "http://www.blocket.se/bostad/uthyres/stockholm?f=p&f=c&f=b"
BLOCKET_FAR_FROM_THE_CITY = ["Botkyrka","Ekerö","Haninge","Huddinge","Lidingö","Järfälla","Norrtälje","Nykvarn","Nynäshamn","Salem",
                 "Sigtuna","Södertälje","Tyresö","Upplands-Bro","Vallentuna","Vaxholm","Värmdö","Österåker"]
BLOCKET_NEAR_THE_CITY = ["Nacka", "Danderyd","Sollentuna","Solna","Sundbyberg","Täby", "Upplands Väsby", "Kista, Hässelby, Vällingby, Spånga"]

class BlocketScraper
  def initialize(mongo_client, logger)
    @logger = logger
    @client = mongo_client
  end

  def scrape
    puts "BlocketScraper Scraping"
    begin
    page = Nokogiri::HTML(open(BLOCKET_STOCKHOLM_URL))

    listings = page.css("#item_list > .item_row")

    rentals = []
    listings.each do |listing|
      text = (listing.css("a").text.strip rescue nil)
      rental = {source: "Blocket",
                url: (listing.css("a").attribute("href").value.strip rescue nil),
                text: text,
                date: (DateTime.parse(listing.css("time").attribute("datetime").value) rescue nil),
                rooms:( listing.css(".first.rooms").text.gsub(" rum", "").gsub(",",".").to_f rescue nil),
                rent: (listing.css(".monthly_rent").text.gsub(" ", "")[/\d+/].to_i rescue nil),
                size: (listing.css(".size").text.gsub(" ", "")[/\d+/].to_i rescue nil),
                location:( listing.css(".subject-param.address").text.strip rescue nil),
                location_type: get_location_type(( listing.css(".subject-param.address").text.strip rescue nil)),
                type: get_type((listing.css(".subject-param.category").text.strip rescue nil), text)
      }
      rental.each {|k,v| @logger.warn "#{k} nil for url #{rental[:url]}" if v.nil?}
      rentals << rental unless rental[:url].nil?
    end

    urls = rentals.map{|r| r[:url] }
    existing_urls = @client[:rentals].find( {url: { "$in" => urls } }).projection(url: 1).to_a.map { |r| r[:url] }

    rentals.find_all { |r| !existing_urls.include? r[:url] }
    rescue
      []
    end
  end

  def get_type(type_text, title)
    if title && title.downcase.include?("rum")
      :room
    elsif type_text && type_text.downcase.include?('radhus') || type_text.downcase.include?("fritidsboende") || type_text.downcase.include?("villa")
      :house
    else
      :apartment
    end
  end

  def get_location_type(location)
    if location
      if BLOCKET_NEAR_THE_CITY.include? location
        return :near_the_city
      elsif BLOCKET_FAR_FROM_THE_CITY.include? location
        return :far_from_the_city
      elsif location.include? "Stockholms stad"
        return :in_the_city
      end
    end
    return :near_the_city
  end
end
