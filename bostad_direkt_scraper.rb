require 'open-uri'
require 'nokogiri'
require 'set'

BOSTAD_DIREKT_STOCKHOLM_URL = "http://www.bostaddirekt.com/Private/Default.aspx?custType=&apmnt=1&other=1&Room=1&RoomsMin=0&SizeMin=0&RentMax=999999&Furnished=-1&PeriodMinMax=min&Period=1&PublDays=0&EstateID=&Areas=7001&SortBy=zip_Name&SortDir=desc&minimize=1"
BOSTAD_DIREKT_BASE_URL= "http://www.bostaddirekt.com/?estateId="
BOSTAD_DIREKT_IN_THE_CITY = ["City", "Djurgården","Gamla stan", "Gärdet","Kungsholmen","Liljeholmen", "Reimersholme", "SÖDERMALM", "Södermalm", "Vasastan", "Östermalm"]
BOSTAD_DIREKT_FAR_FROM_THE_CITY = ["Botkyrka","Ekerö", "Enskede", "Farsta", "Farsta / Ågesta", "Gamla Enskede", "Haninge","Huddinge","Hägersten","Hässelby", "Ingarö", "Järfälla","Kista", "Kummelnäs", "Norrtälje","Nykvarn","Nynäshamn","Salem", "Saltsjö-Duvnäs","Segeltorp", "Skarpnack", "Sollentuna", "Spanga", "Tyresö", 
                 "Upplands-Bro","Vällingby","Värmdö", "Vallentuna","Vaxholm","Värmdö","Österåker"]
BOSTAD_DIREKT_NEAR_THE_CITY = ["Årsta", "Älvsjö", "Bromma", "Bagarmossen", "Bandhagen","Danderyd", "Djursholm", "Enskededalen", "Gröndal","Gullmarsplan", "Hammarby", "Johanneshov", "Nacka", "Solna", "Stora Essingen", "Sundbyberg","Täby", "Upplands Väsby", "Vällingby, Spånga", "Lidingö"]

class BostadDirektScraper
  def initialize(mongo_client, logger)
    @logger = logger
    @client = mongo_client
  end

  def scrape
    puts "BostadDirektScraper Scraping"
    begin
    page = Nokogiri::HTML(open(BOSTAD_DIREKT_STOCKHOLM_URL))

    listings = page.css(".tblResultlist tr[valign='top']")

    rentals = []
    i = 0
    while i < listings.length - 1 do
      header_row = listings[i]
      header_cells = header_row.css("td")
      type = header_cells[1].text.strip
      area = header_cells[2].text.strip
      rooms = header_cells[3].text.strip
      size = header_cells[4].text.strip
      rent = header_cells[5].text.strip
      content = listings[i+1]
      url = (BOSTAD_DIREKT_BASE_URL + content.css(".tblResultlistDetails tr")[0].css('td')[1].text.strip) rescue nil

      rental = {source: "Bostad Direkt",
                url: url,
                text: "#{type} i #{area}",
                date: DateTime.new,
                rooms: rooms.to_f,
                rent: rent.to_i,
                size: size.gsub("m²", "").to_i,
                location: area,
                location_type: get_location_type(area),
                type: get_type(type)
      }
      rental.each {|k,v| @logger.warn "#{k} nil for url #{rental[:url]}" if v.nil?}
      rentals << rental unless rental[:url].nil?
      i += 2
    end

    urls = rentals.map{|r| r[:url] }
    existing_urls = @client[:rentals].find( {url: { "$in" => urls } }).projection(url: 1).to_a.map { |r| r[:url] }

    rentals.find_all { |r| !existing_urls.include? r[:url] }
    rescue
      []
    end
  end

  def get_location_type(location)
    if location
      if BOSTAD_DIREKT_IN_THE_CITY.map(&:downcase).include? location.downcase
        return :in_the_city
      elsif BOSTAD_DIREKT_FAR_FROM_THE_CITY.map(&:downcase).include? location.downcase
        return :far_from_the_city
      elsif BOSTAD_DIREKT_NEAR_THE_CITY.map(&:downcase).include? location.downcase
        return :near_the_city
      end
    end
    return :near_the_city
  end

  def get_type(type)
    if type.downcase.include? 'rum'
      :room
    elsif type.downcase.include? 'hus'
      :house
    else
      :apartment
    end
  end
end
