require 'dotenv'
require 'logger'
require 'mongo'
require 'aws-sdk'
require_relative 'alert_generator.rb'

Dotenv.load

logger = Logger.new('alert-generator.log', 10, 1024000)
mongo_client = Mongo::Client.new(ENV['MONGO_URL'])
aws_client = Aws::SES::Client.new() #reads config from ENV

alert_generator = AlertGenerator.new(mongo_client, aws_client, logger)
alert_generator.run
