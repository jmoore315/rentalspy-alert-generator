require 'set'
require_relative 'mailer.rb'
require_relative 'blocket_scraper.rb'
require_relative 'bostad_direkt_scraper.rb'
require_relative 'hittarum_scraper.rb'
require_relative 'bo_punkten_scraper.rb'


class AlertGenerator
  def initialize(mongo_client, aws_client, logger)
    @logger = logger
    @mongo_client = mongo_client
    @mailer = Mailer.new(aws_client, @logger)
    @blocket_scraper = BlocketScraper.new(@mongo_client, @logger)
    @bostad_direkt_scraper = BostadDirektScraper.new(@mongo_client, @logger)
    @hittarum_scraper = HittarumScraper.new(@mongo_client, @logger)
    @bopunkten_scraper = BoPunktenScraper.new(@mongo_client, @logger)
  end

  def run
    i = 0
    while true do
      new_rentals = @blocket_scraper.scrape + @bostad_direkt_scraper.scrape + @bopunkten_scraper.scrape
      new_rentals += @hittarum_scraper.scrape if i%5 == 0 #hit hittarum every 5 mins
      @logger.info ("New rentals: " + new_rentals.inspect)
      users = get_users
      alerts = generate_alerts_for_users(new_rentals, users)
      send_mail(alerts)
      insert_found_rentals(new_rentals)
      sleep 60
      i+=1
    end
  end

  def insert_found_rentals(rentals)
   result = @mongo_client[:rentals].insert_many(rentals)
   @logger.info "Inserted #{result} new rentals"
  end

  def get_users
    @mongo_client[:users].find({subscription_end_date: {:$gte => Date.today}, is_email_confirmed: true,
                           alerts_enabled:true}).projection(name: 1, email:1, max_rent: 1, in_the_city: 1,
                  near_the_city: 1, far_from_the_city: 1, min_size: 1,
                  min_rooms: 1, room: 1, entire_apartment: 1,
                  blocket: 1, bostad_direkt: 1, hittarum: 1, bopunkten: 1,
                  referral_id: 1
    ).to_a
  end

  def generate_alerts_for_users(rentals, users)
    unique_alert_id = SecureRandom.hex    #unique id to be included in emails to prevent gmail from hiding text
    alerts = []
    users.each do |user|
      user_alert = {user: {email: user[:email], name: user[:name], referral_id: user[:referral_id]}, 
                    unique_id: unique_alert_id,
                    rentals: rentals.find_all { |r| fits_filters?(r, user) } }
      alerts << user_alert unless user_alert[:rentals].empty?
    end
    @logger.info("Alerts to send: " + alerts.inspect)
    alerts
  end

  def fits_filters?(rental, user)
    #source
    return false if !user[:blocket] && rental[:source] == "Blocket"
    return false if !user[:bostad_direkt] && rental[:source] == "Bostad Direkt"
    return false if !user[:hittarum] && rental[:source] == "Hittarum"
    return false if !user[:bopunkten] && rental[:source] == "BoPunkten"

    #size
    return false if (user[:min_size] || 0) > 0 && (rental[:size].nil? || rental[:size] == 0 || rental[:size] < user[:min_size])
    #rooms
    return false if (user[:min_rooms] || 0) > 0 && (rental[:rooms].nil? || rental[:rooms] == 0 || rental[:rooms] < user[:min_rooms])
    #rent
    return false if (user[:max_rent] || 0) > 0 && (rental[:rent].nil? || rental[:rent] == 0 || rental[:rent] > user[:max_rent])

    #location
    # in the city  #TODO check if we can rewrite as !user[rental[:location_type]]
    return false if !user[:in_the_city] && rental[:location_type] == :in_the_city
    # near the city
    return false if !user[:near_the_city] && rental[:location_type] == :near_the_city
    # far from the city
    return false if !user[:far_from_the_city] && rental[:location_type] == :far_from_the_city

    #type
    return false if !user[:room] && rental[:type] == :room
    return false if !user[:entire_apartment]  && rental[:type] == :apartment
    return false if !user[:house] && rental[:type] == :house

    true
  end

  def send_mail(alerts)
    alerts.each do |alert|
      @mailer.send_alert_mail(alert)
    end
  end
end

